---
title: "Merging Policy"
date: 2020-05-12T09:40:42+02:00
---

So you implemented a lovely git branching model, and the result is that your repository has many ``feature/*`` branches instead of just many branches? And you're sad because it didn't solve your problem.

Instead of being angry with the branching model, what about asking what *problem* does it solve? The answer would be *the problem of how to create branches*. And here we are.

I want to emphasize that a branching model only prepares your repository for *merging*. If not, you didn't understand.

**Make sure the team understand the goal is to merge.**

So that's it. Write down the conditions for accepting new code. *Branching model* tells how to start contribution, *merging policy* tells how to finish it.
