---
title: "Basic Git Commands (Not Git Basics)"
date: 2020-10-02T23:29:27+02:00
---

There are a few git commands I use all the time. I want to share them.


``git checkout -b BRANCH``, ``git checkout BRANCH``
    Create a new feature or switch between two.


``git status``, ``git diff``, ``git diff --cached``
    To get an idea about what to commit.


``git add -p FILE``
    Mark file changes for commit (stage them.)


``git commit -m'MESSAGE'``, ``git commit --amend``
    Commit changes, merge with the previous commit.


``git log --oneline --graph --decorate --all``
    Show the repository history the way I can understand. I have an alias for this.


``git show COMMIT``
    Show particular commit changes.


``git stash``, ``git stash pop``
    Sometimes needed before/after rebase.


``git rebase -i BRANCH_OR_COMMIT``
    Take the current branch and put it on top of ``BRANCH_OR_COMMIT``. Do it
    iteratively, so change what needs to be changed.


``git merge --no-ff BRANCH``
    Merge the ``BRANCH`` to the current branch. However, do not meld it.


Do not forget that all the git magic is damned if you don't know `how to write
a git commit message`_.

.. _how to write a git commit message: https://chris.beams.io/posts/git-commit/
