+++
title = "Git intro workshop"
date = 2018-10-11T10:41:10+02:00
tags = []
categories = []
+++

{{% section %}}
# Git intro workshop
[Jiri Hubacek](https://qeef.gitlab.io/)

{{% /section %}}
{{% section %}}
# We are going to...
- Clone repository.
- Explore that repository.
- Create new repository.
- Check repository current state.
  - Understand states of patches.
- Build repository history.

{{% /section %}}
{{% section %}}
# Clone repository
From [rtime gitweb][1] clone the repository.

`git clone ssh://git@rtime.felk.cvut.cz/hubacji1/oneflow.git`

[1]: https://rtime.felk.cvut.cz/gitweb/hubacji1/oneflow.git

{{% /section %}}
{{% section %}}
# Explore repository
- `git log`
- `git log --graph`
- `git log --oneline`
- `git log --decorate`
- `git show COMMIT`

{{% /section %}}
{{% section %}}
# Create new repository
- `git init`
- `git config user.name "Your Name"`
- `git config user.email you@example.com`
- `git config --global user.name "Your Name"`
- `git config --global user.email you@example.com`

{{% /section %}}
{{% section %}}
# Check repository current state
- `git status`
- `git diff`
- `git diff --cached`

{{% /section %}}
{{% section %}}
## States of patches
- *Patch* is hunk of file change.
- There are 3 states of patches:
  - untracked,
  - unstaged,
  - staged.
- There are following transitions:
  - untracked, unstaged -> `add` -> staged;
  - staged -> `reset` -> untracked, unstaged;
  - staged -> `commit` -> git history.
- Draw it!

{{% /section %}}
{{% section %}}
# Build repository history
- `git add -p FILE`
- `git add FILE`
- `git reset -p FILE`
- `git reset FILE`
- `git commit -m'COMMIT MSG'`
- `git commit`

{{% /section %}}
{{% section %}}
# That's all Folks
## Read for sure
- https://chris.beams.io/posts/git-commit/
- https://www.sbf5.com/~cduan/technical/git/git-1.shtml

## Just for reference
- https://git-scm.com/
- https://githowto.com/

{{% /section %}}
