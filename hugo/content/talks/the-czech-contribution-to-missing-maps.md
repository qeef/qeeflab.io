+++
title = "The Czech Contribution to Missing Maps"
date = 2018-05-30T19:00:49+02:00
tags = ["mapathon", ]
categories = []
+++

{{% section %}}
# The Czech Contribution to Missing Maps
[Jiri Hubacek](https://qeef.gitlab.io/)
{{% /section %}}
{{% section %}}
# Who is the community?
People who usually go to the pub after mapathon.
{{% /section %}}
{{% section %}}
# How does the community contribute?
- The Czech translations of [Missing Maps], [Highway Tag Africa].
- Contribution to [JOSM] core.
- [Mapathoner] plugin for [JOSM].

[Missing Maps]: http://missingmaps.org/cs/
[Highway Tag Africa]: https://wiki.openstreetmap.org/wiki/Cs:Highway_Tag_Africa
[JOSM]: http://josm.openstreetmap.de/
[Mapathoner]: https://qeef.github.io/mapathoner/
{{% /section %}}
{{% section %}}
## Translation of [Missing Maps] ...
Available for non-english speaking mappers in CZ & SK.

Thanks to Jan Bohm, Jiri Podhorecky, Lukas Vit, Veru Kotková, Tereza Botková,
Janka Joe Smutná.

## ... and [Highway Tag Africa]
Thanks to Janka Joe Smutná.

[Missing Maps]: http://missingmaps.org/cs/
[Highway Tag Africa]: https://wiki.openstreetmap.org/wiki/Cs:Highway_Tag_Africa
{{% /section %}}
{{% section %}}
# Contribution to [JOSM] core
- Introduce [almost square check for buildings].
- [Crossing of residential areas with something].
- WIP: [Check for buildings sharing nodes with res. area or ways].
- WIP: Add [BuildingOutsideResidentialArea] test.
- WIP: New validation for [broken circle buildings].

Thanks to Martin Liska.

[JOSM]: http://josm.openstreetmap.de/
[almost square check for buildings]: https://josm.openstreetmap.de/ticket/16189
[Crossing of residential areas with something]: https://josm.openstreetmap.de/ticket/16188
[Check for buildings sharing nodes with res. area or ways]: https://josm.openstreetmap.de/ticket/16261
[BuildingOutsideResidentialArea]: https://josm.openstreetmap.de/ticket/16262
[broken circle buildings]: https://josm.openstreetmap.de/ticket/16283
{{% /section %}}
{{% section %}}
# [Mapathoner] plugin for [JOSM]
- Create multiple buildings by one way.
- Create residential area by selecting the buildings.

Thanks to [these guys]. And me.

[Mapathoner]: https://qeef.github.io/mapathoner/
[JOSM]: http://josm.openstreetmap.de/
[these guys]: https://qeef.github.io/mapathoner/credits/
{{% /section %}}
{{% section %}}
# So, like that...
{{% /section %}}
