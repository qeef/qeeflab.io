+++
title = "Git branching workshop"
date = 2018-10-15T17:30:10+02:00
tags = []
categories = []
+++

{{% section %}}
# Git branching workshop
[Jiri Hubacek](https://qeef.gitlab.io/)

{{% /section %}}
{{% section %}}
# We are going to...
- Clone repository and explore it.
  - Checkout and reset to history.
  - List branches.
- Build feature.
  - Create branch.
  - Commit to branch.
  - Merge the branch.
  - Interactive rebase.
- Merging.
  - Fast-forward merge.
  - Parallel history.
- Conflicts.
  - Create conflict.
  - Solve by merge.
  - Solve by rebase.

{{% /section %}}
{{% section %}}
# Clone repository
From [rtime gitweb][1] clone the repository.

`git clone ssh://git@rtime.felk.cvut.cz/hubacji1/oneflow.git`

## Or pull it

`git pull --rebase`

## And explore with
- `git log --oneline --graph`
- `git config --global alias.logg log --oneline --graph`

[1]: https://rtime.felk.cvut.cz/gitweb/hubacji1/oneflow.git

{{% /section %}}
{{% section %}}
# Checkout to history
- `git checkout COMMIT_ID`
- `git checkout master`

# Reset to history
- `git reset COMMIT_ID`
- `git reset --hard COMMIT_ID`

## Note
Commit all the changes before playing around with git history. It means that
`git status` says *nothing to commit, working tree clean*.

{{% /section %}}
{{% section %}}
# List branches
- `git branch`
- `git branch --all`
- `git log --oneline --graph --decorate`
- `git log --oneline --graph --decorate --all`

{{% /section %}}
{{% section %}}
# Build feature
- Build feature.
- Interactive rebase.

{{% /section %}}
{{% section %}}
## Build feature
- `git branch BRANCH`
- `git checkout BRANCH`
- build history in feature branch
- checkout to master branch
- `git merge --no-ff BRANCH`

{{% /section %}}
{{% section %}}
## Interactive rebase
- create feature branch
- `git branch BRANCH`
- `git checkout BRANCH`
- build history in feature branch
- add *FIX* commits
- interactive rebase
- `git rebase -i COMMIT_ID`
- checkout to master branch
- `git merge --no-ff BRANCH`

{{% /section %}}
{{% section %}}
# Merging
- Fast-forward merge.
- Parallel history.

{{% /section %}}
{{% section %}}
# Merge branches
- merge commit
- use to join branches
- `git merge BRANCH`
- `git merge --no-ff BRANCH`

{{% /section %}}
{{% section %}}
## Fast-forward merge
- reset the last (merge) commit
- merge feature branch again
- `git merge BRANCH`

{{% /section %}}
{{% section %}}
## Parallel history
- create feature branch
- build history in feature branch
- checkout to master branch
- build history in master branch
- merge feature branch
- `git merge BRANCH`

{{% /section %}}
{{% section %}}
# Conflicts
- Create conflict.
- Solve by merge.
- Solve by rebase.

{{% /section %}}
{{% section %}}
## How to create conflict
- create feature branch
- update file
- checkout to master
- update that file again

{{% /section %}}
{{% section %}}
## Solve conflict by merge
- checkout to master
- merge feature branch
- solve conflicts

{{% /section %}}
{{% section %}}
## Solve conflict by rebase
- drop last (merge) commit
- checkout to feature branch
- rebase to master
- solve conflicts during rebase
- checkout to master
- merge feature branch

{{% /section %}}
{{% section %}}
# No time for questions
## There is always time for questions...
- Why *NOT* to use `merge` but `rebase`?

## ... and discussion ofcourse ...
- Rebase forces you to keep your *feature* updated with *master*.

## ... and some forgotten notes
- Remote is some branch on server.
- `git pull` = `git fetch` & `git merge`.
  - `git fetch` gets the remote branch.
- Fix `git pull` := `git fetch` & `git rebase` with:

```
git config --global branch.autosetuprebase always
```

{{% /section %}}
