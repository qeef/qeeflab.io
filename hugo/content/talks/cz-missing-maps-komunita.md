+++
title = "[CZ] Missing Maps komunita"
date = 2019-07-02T09:27:24+02:00
tags = ["mapathon", ]
categories = []
+++

{{% section %}}
# Missing Maps komunita
[Jiří Vlasák](https://qeef.gitlab.io/)
{{% /section %}}

{{% section %}}
# Co já?
- 3 roky na mapathonech.
- Jednou napsal Honza, že nemá kdo školit iD.
- Nastavení Trella jako veřejné nástěnky.
{{% /section %}}

{{% section %}}
# Co mappeři?
- Jsou mapy potřeba?
- Proč nepoužít GoogleMaps?
- Nemůže mapovat umělá inteligence?
{{% /section %}}

{{% section %}}
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/d/db/Hand_drawn_MSF_map_1.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/7e/Hand_drawn_MSF_map_2.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/4/48/Hand_drawn_MSF_map_3.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/76/Hand_drawn_MSF_map_4.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/e/e1/Hand_drawn_MSF_map_6.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/72/Hand_drawn_MSF_map_7.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/4/49/Hand_drawn_MSF_Map_5.png" height="100%" />
</div>
{{% /section %}}

{{% section %}}
<video width="100%" height="100%" data-autoplay><source
    data-src="https://video.twimg.com/ext_tw_video/1085939215182061568/pu/vid/1280x720/p2KHJ-RzeWr8AuYn.mp4?tag=6"
    type="video/mp4"
/></video>
{{% /section %}}

{{% section %}}
<!--
Karen Zack's twitter
@see https://twitter.com/teenybiscuit/status/707727863571582978/photo/1
@see https://www.dailymail.co.uk/femail/article-3486542/Furry-friend-tasty-treat-Woman-leaves-internet-awe-images-revealing-remarkable-similarities-dogs-food-puppies-bagels-Labradoodles-fried-chicken.html
-->
<div class="slide" style="text-align:center">
  <img src="https://i.dailymail.co.uk/i/pix/2016/03/10/22/3211B14100000578-3486542-Just_face_it_The_posts_which_also_includes_this_comparison_of_ch-m-45_1457650715884.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://i.dailymail.co.uk/i/pix/2016/03/10/22/32132ECF00000578-3486542-Cleaning_up_Karen_hasn_t_restricted_herself_to_food_comparisons_-m-46_1457650743618.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://i.dailymail.co.uk/i/pix/2016/03/10/22/3211B14600000578-3486542-All_rolled_up_Plain_sesame_and_even_pumpernickel_bagels_are_imit-a-44_1457649733075.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://i.dailymail.co.uk/i/pix/2016/03/10/22/3211B14D00000578-3486542-Pawesome_meme_A_Portland_woman_s_compiled_galleries_of_dogs_and_-a-43_1457649732886.jpg" height="100%" />
</div>
{{% /section %}}

{{% section %}}
# Co komunita?
- O mappery se někdo musí starat.
- Missing Maps je hub.
- Komunita.
{{% /section %}}

{{% section %}}
<!--
OpenStreetMap wiki
@see https://wiki.openstreetmap.org/
-->
<div style="height:100%">
<img
    src="https://wiki.openstreetmap.org/w/images/9/9f/Missing_maps8_A4.png"
    height="100%"
/>
<div>
{{% /section %}}
