+++
title = "Divide and map. Now."
date = 2020-01-26T13:02:57+01:00
tags = ["fosdem"]
categories = []
+++

{{% section %}}
# Divide and map. Now.
[Jiri Vlasak](https://qeef.gitlab.io/)
{{% /section %}}

{{% section %}}
# [Missing Maps](http://www.missingmaps.org/)
- volunteering for 3 years
- contributing to [OpenStreetMap](https://www.openstreetmap.org/)
- we are mapping developing countries
- maps used by humanitarian organizations (MSF, Red Cross) and locals
{{% /section %}}

{{% section %}}
# Mapathon
- Many dedicated mappers (usually 20 - 100)
- Large area to map

# What is the approach?
- Divide the area to the smaller squares that a human can map
- Let the dedicated mappers map the squares
{{% /section %}}

{{% section %}}
# Sounds familiar?
- [HOT Tasking Manager](https://tasks.hotosm.org/)
  - [Humanitarian OpenStreetMap Team](https://www.hotosm.org/)
  - I experienced transition from `v2` to `v3` as user
  - Looking around last few months -- `v4`
- [Mapping North Korea](https://mappingnorthkorea.com/map)
{{% /section %}}

{{% section %}}
# Some of the HOT Tasking Manager issues
- Primary functionality first
- Performance issues (the project architecture)
- Project management vs. community driven
- Closed tools used (Google disk, Slack, ...)

# They do it wrong
- In my humble opinion, it may look like there are some little issues, I think.
- I want to provide constructive critique/feedback.
{{% /section %}}

{{% section %}}
# [Divide and map. Now.](https://www.damn-project.org/)
- Multiple repositories (explicitly splitted)
  - Server -- JSON API to [PostGIS](https://postgis.net/) database
  - Client -- for mappers (number of mouse clicks to start mapping)
  - Manager -- create area to map: [MapSwipe Analytics](https://mapswipe.heigit.org/analytics/), [geojson.io](http://geojson.io)
  - [JOSM](https://josm.openstreetmap.de/) plugin -- of course
  - Deploy -- put the project to production
- Gitter chat (archived, open communication channel)
  - Mappers <--> Client devs <--> Server devs
- GitLab Issues
  - What? Why? --> How?
{{% /section %}}

{{% section %}}
# Damn server
- FastAPI -- asyncio REST JSON API
- [PostGIS](https://postgis.net/) database
- Areas, squares, commits, users
- Map/review square as database query
- Split area/square written in [plpgsql](https://www.postgresql.org/docs/current/plpgsql.html)
- Since for commits
{{% /section %}}

{{% section %}}
# Damn clients
- Proof of concept
- Static web page
- [Client library](http://client.damn-project.org/lib.js)
- New clients more than welcome

# Damn deploy
- change environment variables in `env` file for specific domain
- `docker-compose up`
{{% /section %}}

{{% section %}}
# The damn future
- Use by some local community of mappers
- Community to drive the project
- Benchmarking to HOT Tasking Manager
  - Server currently runs on `1 vCPUs, 1GB / 25GB Disk`
- Profiling <--> refactor loop
{{% /section %}}

{{% section %}}
<center>
<a href="https://www.damn-project.org/" target="_blank">
<img
  src="https://www.damn-project.org/damn.svg"
  width="40%"
/>
<h1 style="margin-top: 0px">
  www.damn-project.org
</h1>
</a>
</center>
{{% /section %}}
