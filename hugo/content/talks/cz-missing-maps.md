+++
title = "[cz] Missing Maps"
date = 2019-10-24T07:04:01+02:00
tags = []
categories = []
+++

{{% section %}}
# Missing Maps
[Jiří Vlasák](https://qeef.gitlab.io/)
{{% /section %}}

{{% section %}}
# [Zemětřesení na Haiti](https://en.wikipedia.org/wiki/2010_Haiti_earthquake)
- 12. 1. 2010
- zničeno 250 000 obytných domů a 30 000 komerčních budov
- poničeny komunikační systémy, doprava, nemocnice, elektrická síť
- humanitární pomoc z celého světa
  - [Organizace spojených národů (OSN)](https://cs.wikipedia.org/wiki/Organizace_spojen%C3%BDch_n%C3%A1rod%C5%AF)
  - [Lékaři bez hranic (MSF)](https://cs.wikipedia.org/wiki/L%C3%A9ka%C5%99i_bez_hranic)
  - [Mezinárodní červený kříž](https://cs.wikipedia.org/wiki/Mezin%C3%A1rodn%C3%AD_%C4%8Derven%C3%BD_k%C5%99%C3%AD%C5%BE)
- zapojení OpenStreetMap komunity
  - zlepšení map v oblasti na základě satelitních snímků
  - crowdmapping -- kombinace sociálních médií, komunikační kanalů a
    geografických dat do digitální mapy ([Ushahidi](https://www.ushahidi.com/))

{{% /section %}}

{{% section %}}
<div class="slide" style="text-align:center">
<iframe width="100%" height="100%" frameborder="0" scrolling="no"
marginheight="0" marginwidth="0"
src="https://www.openstreetmap.org/export/embed.html?bbox=-146.95312500000003%2C-46.43785689502421%2C-41.83593750000001%2C67.13582938531948&amp;layer=mapnik"
style="border: 1px solid black"></iframe><br/><small><a
href="https://www.openstreetmap.org/#map=3/19.15/-94.39">View Larger
Map</a></small>
</div>

<div class="slide" style="text-align:center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/e/eb/Haiti_Jan2010_ShakeMap.png" height="100%" />
</div>

<div class="slide" style="text-align:center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/2/2f/Haiti_Earthquake_building_damage.jpg" height="100%" />
</div>

<div class="slide" style="text-align:center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/c/cc/Earthquake_damage_in_Jacmel_2010-01-17_4.jpg" height="100%" />
</div>

<div class="slide" style="text-align:center">
<iframe width="100%" height="100%"
src="https://www.youtube.com/embed/e89Tqr75mMw" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>
</div>
{{% /section %}}

{{% section %}}
# HOT
- Humanitarian OpenStreetMap Team
- založen po zemětřesení na Haiti
- humanitární pomoc a pomoc v rozvojových zemích (skrze mapování)
- pro pomoc v oblastni jsou mapy potřeba

{{% /section %}}

{{% section %}}
# Proč jsou mapy potřeba?

{{% /section %}}

{{% section %}}
# [Proč jsou mapy potřeba?](https://www.linkedin.com/pulse/20141109113719-116540745-how-can-ngos-use-gis-and-mapping)
- orientace v lokalitě -- co je kolem?
- jak se dostat z místa A do místa B?
- sdílení polohových dat -- nemocnice, pitná voda, obydlené oblasti
- plánování záchraných operací
- plánování preventivních kampaní (např. očkování)
- vizualizace statistik -- obrázek je lepší než tisíc slov

{{% /section %}}

{{% section %}}
# [Lékaři bez hranic (MSF)](https://cs.wikipedia.org/wiki/L%C3%A9ka%C5%99i_bez_hranic)
- sekulární humanitární nezisková organizace
- celosvětová organizace
- založena 22. 12. 1971
- financování ze soukromých zdrojů (nezávislé na vládách)

## Poslání
- poskytovat zdravotní péči
- přinášet svědectví

{{% /section %}}

{{% section %}}
# Některé z misí Lékařů bez hranic
- 2010, Haiti: přírodní katastrova
  - během 5 měsíců 173.757 pacientů
  - a 11.748 operací
- 2012, Guinea: očkovací kampaň
  - očkování proti choleře
  - 117 tisíc lidí
- 2013, Jižní Súdán: konflikt
  - 21 tisíc vysídlených obyvatel
  - kritika OSN
- 2013, Somálsko: ukončení projektů v zemi
  - útoky na členy MSF, výhrůžky, únosy a vraždy
  - jedno z nejtěžších rozhodnutí v dějinách organizace
- 2014: Missing Maps
  - mapování nejzranitelnějších míst světa

{{% /section %}}

{{% section %}}
# Některé z misí Lékařů bez hranic
- 2014, západní Afrika: boj proti Ebole
  - 15 center pro léčbu Eboly
  - ošetří 5 tisíc pacientů
- 2015, Středozemní moře: migrační krize
  - zdravotní péče
- 2015, Ukrajina: konflikt
  - zdravotní péče
  - psychycká podpora
  - léky
  - pro lidi na obou stranách konfliktu
- 2016, Sýrie a Jemen: konflikt
  - útoky na civilisty, nemocnice, zdravotnická zařízení a jejich pracovníky

{{% /section %}}

{{% section %}}
# Mapy pro Lékaře bez hranic
- kam dojet za pacientem?
- jak rychle se šíří nemoc?
- kde začít očkovat?
- postřik proti Malárii

{{% /section %}}

{{% section %}}
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/d/db/Hand_drawn_MSF_map_1.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/7e/Hand_drawn_MSF_map_2.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/4/48/Hand_drawn_MSF_map_3.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/76/Hand_drawn_MSF_map_4.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/e/e1/Hand_drawn_MSF_map_6.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/7/72/Hand_drawn_MSF_map_7.jpg" height="100%" />
</div>
{{% /section %}}

{{% section %}}
# OpenStreetMap
- komerční firmě se mapy v rozvojových zemích nevyplatí
  - a ještě licence
- jak to vypadá v Zimbabwe?
  - [Google Maps](https://www.google.com/maps/place/22%C2%B004'08.6%22S+29%C2%B017'53.2%22E/@-22.0690646,29.2973259,18.59z/data=!4m5!3m4!1s0x0:0x0!8m2!3d-22.0690457!4d29.2981031)
  - [OpenStreetMap](https://www.openstreetmap.org/query?lat=-22.06907&lon=29.29813)
  - [Mapy.cz](https://en.mapy.cz/zakladni?x=29.2981031&y=-22.0689860&z=17&source=coor&id=29.2981031%2C-22.0690457)
- OpenStreetMap jako Wikipedie
  - kdokoli může přispět (polohy nemocnic, názvy vesnic)
  - kdokoli může použít (nejen MSF)

{{% /section %}}

{{% section %}}
# [OpenStreetMap v České republice](https://openstreetmap.cz/)
- [OpenStreetMap](https://www.openstreetmap.org/node/296766437)
- [Mapy.cz](https://en.mapy.cz/s/gepesusufo)
- [Google Maps](https://goo.gl/maps/GKhQWWeSzD1yeL8J6)

{{% /section %}}

{{% section %}}
# OpenStreetMap v Android telefonu
- [OsmAnd](https://osmand.net/)
- [Vespucci](http://vespucci.io/)
- [StreetComplete](https://wiki.openstreetmap.org/wiki/StreetComplete)

{{% /section %}}

{{% section %}}
# Missing Maps
- projekt Amerického a Britského červeného kříže, HOT a MSF
- preventivní mapování
- přidávání nejvíce ohrožených míst světa do OpenStreetMap

{{% /section %}}

{{% section %}}
<div class="slide" style="text-align:center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/c/ce/Haitian_national_palace_earthquake.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/d/df/HOT_Tasking_Server_Job.png" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/5/5c/2015_04_26_Nepal_Earthquake_Kathmandu_living_lab.png" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/e/e7/OpenStreetMap_on_a_Garmin_in_Haiti.JPG" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/c/ca/LA_County_SAR_pulls_Haitian_woman_from_earthquake_debris_2010-01-17.jpg" height="100%" />
</div>

{{% /section %}}

{{% section %}}
vaccine campaign
<div class="slide" style="text-align:center">
<iframe width="100%" height="100%"
src="https://www.youtube.com/embed/mwRdtnfFcUw" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>
</div>
<div class="slide" style="text-align:center">
  <img src="https://cdn-images-1.medium.com/max/1600/1*N2Ef_IO5tsMWj5PHIyzrmQ.jpeg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://pbs.twimg.com/media/CrnOrK-WgAE__Ra?format=jpg&name=large" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/d/df/HOT_Tasking_Server_Job.png" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://wiki.openstreetmap.org/w/images/9/92/Editathon-portland.jpg" height="100%" />
</div>
<div class="slide" style="text-align:center">
  <img src="https://cdn-images-1.medium.com/max/1600/1*nfiveq86EQ-P_EkDD_XqQA.jpeg" height="100%" />
</div>

{{% /section %}}

{{% section %}}
# Mapování
- [MapSwipe](https://mapswipe.org/)
- [iD editor](https://www.openstreetmap.org/#map=18/33.35580/35.38946)

{{% /section %}}

{{% section %}}
<!--
OpenStreetMap wiki
@see https://wiki.openstreetmap.org/
-->
<div style="height:100%">
<img
    src="https://wiki.openstreetmap.org/w/images/9/9f/Missing_maps8_A4.png"
    height="100%"
/>
<div>
{{% /section %}}
