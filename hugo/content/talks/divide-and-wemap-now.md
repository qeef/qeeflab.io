+++ 
title = "Divide and #WeMap. Now."
date = 2020-06-15T22:58:45+02:00
+++

{{% section %}}
# Divide and #WeMap. Now.
[@qeef@en.osm.town](https://en.osm.town/web/accounts/15447)
{{% /section %}}

{{% section %}}
# 30 minutes
- Why the damn project?
- How the damn project?
- Hands on
    - Web client for managers
    - Web client for mappers
    - JOSM plugin for mappers
- Deploy remark
- Answers for your questions
{{% /section %}}

{{% section %}}
# Why the damn project?
- Inspired by [HOT Tasking Manager](https://tasks.hotosm.org/)
- Performance, community, and openness issues
    - Email address and [4%](https://github.com/hotosm/tasking-manager/issues/1243)
- I wanted to contribute to the TM, but...
- ... and the primary function (divide and map) is not so hard problem
{{% /section %}}

{{% section %}}
# How the damn project?
## Non-technical
- The community drives the damn project
- Become a good community member
- Learn to be efficient
- Help others

## Technical
- Divide up functionality, divide up repositories
- Multiple clients for multiple mapper groups
- Make the clients fit mapper needs
- Mappers <–> Client devs <–> Server devs
{{% /section %}}

{{% section %}}
# Hands on
## Manager
- https://manager.damn-project.org/
- split when adding new area
    - minimum square size is approximately `400 m`
    - divided to maximum of `32 x 32 = 1024` squares
- history

## Client
- https://client.damn-project.org/
- translate

## Plugin
- F12 -> Plugins -> Search: damn -> OK
- I think I reached my damn limits
{{% /section %}}

{{% section %}}
# Deploy remark
- https://gitlab.com/damn-project/damn_deploy/
- step by step guide
- deploy your own instance
{{% /section %}}

{{% section %}}
# Answers for your questions
<center>
<a href="https://www.damn-project.org/" target="_blank">
<img
  src="https://www.damn-project.org/damn.svg"
  width="40%"
/>
<h1 style="margin-top: 0px">
  www.damn-project.org
</h1>
</a>
</center>
{{% /section %}}
