+++
title = "SotM 2020 lightning talk"
date = 2020-06-26T16:45:41+02:00
tags = []
categories = []
+++

{{% section %}}
# Divide and map. Now.
the damn project

[Jiri Vlasak](https://qeef.gitlab.io/)
{{% /section %}}

{{% section %}}
# Big area to map?
- Divide that area into smaller squares.
- Let mappers map one by one in parallel.
- You are done.
{{% /section %}}

{{% section %}}
# Why am I not talking about
[HOT](https://www.hotosm.org/) [Tasking Manager](https://tasks.hotosm.org/)?

- Google drive and Slack
- Email address and [4%](https://github.com/hotosm/tasking-manager/issues/1243)
- Performance
{{% /section %}}

{{% section %}}
# The idea is (and why?)
- Divide functionality
    - Support  multiple clients (for multiple mappers needs)
    - Let developers concentrate
    - Communicate efficiently (mappers <-> client devs <-> server devs)
- Make it simple
    - Motivate to contribute (discuss, propose, develop)
    - Simple is efficient (in most cases)
{{% /section %}}

{{% section %}}
# What to expect ...
... after 10 months of development in spare time?

- Python (FastAPI, asyncio, REST, JSON, PostGIS) [server](https://server.damn-project.org/)
- Simple web [client](https://client.damn-project.org/) for mappers
- Simple web JSON-based [manager](https://manager.damn-project.org/) (area history and rollback)
- Simple web [manager](https://damn-project.gitlab.io/damn_manager/)
- `damn` [JOSM plugin](https://josm.openstreetmap.de/wiki/Plugins)
- [Deployment guide](https://gitlab.com/damn-project/damn_deploy)
{{% /section %}}

{{% section %}}
# Deployment guide?
- Separate [repository](https://gitlab.com/damn-project/damn_deploy) for admins
- Step-by-step guide (I use it regularly when updating [server](https://server.damn-project.org/))
- Just docker files and environment variables
{{% /section %}}

{{% section %}}
# Running $5/month instance
- I did [load testing](https://www.openstreetmap.org/user/qeef/diary/393240)
- 100 mappers on (virtual testing) mapathon
- rough mapper's behavior:
    - take square
    - map for 1 - 5 seconds
    - wait for 1 - 2 seconds and take next square
- 6 seconds in average, the median was 3 seconds, no error
- `:)` ? `:(`
    - Debian 10.2 x64
    - 1 vCPU
    - 1GB RAM
    - 25GB (SSD, I think)
{{% /section %}}

{{% section %}}
<center>
<a href="https://www.damn-project.org/" target="_blank">
<img
  src="https://www.damn-project.org/damn.svg"
  width="40%"
/>
<h1 style="margin-top: 0px">
  www.damn-project.org
</h1>
</a>
</center>
{{% /section %}}
