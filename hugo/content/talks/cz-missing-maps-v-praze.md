+++
title = "[cz] Missing Maps v Praze"
date = 2019-11-03T07:25:58+01:00
tags = []
categories = []
+++

{{% section %}}
# Missing Maps v Praze
[Jiří Vlasák](https://qeef.gitlab.io/)
{{% /section %}}

{{% section %}}
# Co že to je  [MissingMaps](http://missingmaps.org/)?
- zakreslování nejzranitelnější míst na světě do map
- [Lékaři bez hranic](https://www.lekari-bez-hranic.cz/), [Red
  Cross](http://redcross.org/)
- jak to vypadá v Zimbabwe?
  - [Google Maps](https://www.google.com/maps/place/22%C2%B004'08.6%22S+29%C2%B017'53.2%22E/@-22.0690646,29.2973259,18.59z/data=!4m5!3m4!1s0x0:0x0!8m2!3d-22.0690457!4d29.2981031)
  - [OpenStreetMap](https://www.openstreetmap.org/query?lat=-22.06907&lon=29.29813)

{{% /section %}}

{{% section %}}
# Nejen pražské milníky
- mapathony odstartoval Honza z MSF (léto 2016)
- překlad [Highway Tag
  Africa](https://wiki.openstreetmap.org/wiki/Cs:Highway_Tag_Africa) a
  [MissingMaps.org](http://missingmaps.org/cs/) (léto 2017)
- [Trello](https://trello.com/b/8Vo5QWU6/missing-maps-cz-sk) je public --
  výsledek [nápadu
  Pavla](https://trello.com/c/Nl4uJBGT/59-mm-cz-sk-jako-skute%C4%8Dn%C4%9B-otev%C5%99en%C3%A1-komunita)
  (léto 2018)
- [mapathon.cz](http://mapathon.cz/) (zima 2019)
- malé mapathony v Praze (léto 2019)
- hackathon v Plzni (podzim 2019)

{{% /section %}}

{{% section %}}
# Mappeři + core tým = komunita
- mapper: "Přidáváme do map nejzranitelnější místa světa."
  - jak zvětšit počet mapperů?
  - facebook
  - kamarádi kamarádů
- core tým: "O mappery se musí někdo starat."
  - jak zvětšit core tým?
  - lidi, co jdou po mapathonu do hospody
  - osobně

{{% /section %}}

{{% section %}}
# Nevděčná práce člena core týmu
- neúnavné povzbuzování ostatních členů core týmu
- mladší členové mají přednost při plnění úkolů
  - když nebudou mít co dělat, nezapojí se
  - ale nenutit
- každý člen core týmu by měl být nahraditelný
  - a pracovat na tom, aby byl
- a co za to?
  - nic
  - fungující core tým -> fungující komunita

{{% /section %}}

{{% section %}}
# Právě probíhající milník v Praze
- od začátku koordinuje mapathony Honza
- zkoušeli jsme mapathon bez koordinátora
- zkoušeli jsme koordinátora per mapathon
- aktuálně řešíme mapathony vždycky pozdě

{{% /section %}}

{{% section %}}
# Koordinátor
- vytvoří včas kartu mapathonu
  - šablona na Trellu
- dohlédne na to, aby byla včas poslaná pozvánka
  - potřebuje přístup k pozvánce
  - potřebuje znát statistiky
  - potřebuje přístup ke komunikačním kanálům
- dohlédne na úkoly, které mají být obsazeny
  - Trello -- proč?
  - povzbuzuje ostatní, aby si rozebrali úkoly

{{% /section %}}

{{% section %}}
# Koordinátor města
- zodpovědnost za mapathony ve městě
- budování komunity
  - povzbuzování core týmu
  - zastupitelnost/nahraditelnost
- kdo je koordinátor města ve vašem core týmu?

{{% /section %}}

{{% section %}}
# Komunita žije
- obměna core týmu v Praze
  - a jak v ostatních městech?
- *ALE* koordinátor
- zdravá komunita = obměna core týmu = nahraditelnost

{{% /section %}}

{{% section %}}
# Mapathony a co dál?
- mapathony
  - mapování
  - vzdělávání
  - rozšiřování mapperů
  - rozšiřování core týmu
- malé mapathony
- překlady
- hackathony

{{% /section %}}
