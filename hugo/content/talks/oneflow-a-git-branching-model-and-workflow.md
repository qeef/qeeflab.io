+++
title = "Oneflow a Git Branching Model and Workflow"
date = 2018-06-27T14:17:14+02:00
tags = []
categories = []
+++

{{% section %}}
# OneFlow - a Git branching model and workflow

http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow

Adam Ruka

{{% /section %}}
{{% section %}}
# Purpose
- Cooperate and keep clean history.
- Standardize feature addition and release processes.

{{% /section %}}
{{% section %}}
# Outline
- First commit:
    - `README.md`
    - `LICENSE`
    - `CHANGELOG.md`
- Workflow:
    - Features.
    - Releases.
    - Hotfixes.

{{% /section %}}
{{% section %}}
## Readme
- Why to add `README.md`?
- *What* is the project about.
- *How* to contribute.
- *How* to run the project.
- *What* dependencies needed.
- Also benefits the owner.

{{% /section %}}
{{% section %}}
## License
- Why to add `LICENSE`?
- Using project by others.

{{% /section %}}
{{% section %}}
## Changelog
- Why to keep a `CHANGELOG.md`?
- What is Semantic Versioning?
    - `X.Y.Z`
    - Major (backward incompatible).
    - Minor (feature).
    - Patch (hotfix).

{{% /section %}}
{{% section %}}
# Workflow
- Features.
- Releases.
- Hotfixes.

{{% /section %}}
{{% section %}}
# How not?
![Branching workflow: git-flow and github-flow | Numergent][p1]

[p1]: http://numergent.com/images/dev/git-flow.png

{{% /section %}}
{{% section %}}
# Why not?
![Gitflow mess][p2]

[p2]: https://www.endoflineblog.com/assets/gitflow-mess.png

{{% /section %}}
{{% section %}}
# Why not?
- Hell a lot of long-live branches.
    - More than 1 is hell a lot.
- Too complicated.
- Too chaotic.

{{% /section %}}
{{% section %}}
# OneFlow advantages
- Single long-live branch, therefore simple.
- Clean history, therefore useful.

{{% /section %}}
{{% section %}}
# How can it look like?
![Git OneFlow Example][p2]

[p2]: https://upload.wikimedia.org/wikipedia/commons/a/a9/OneFlow_Example.png

{{% /section %}}
{{% section %}}
# GitFlow/OneFlow comparison
![Branching workflow: git-flow and github-flow | Numergent][p1]
![Git OneFlow Example][p2]

[p1]: http://numergent.com/images/dev/git-flow.png
[p2]: https://upload.wikimedia.org/wikipedia/commons/a/a9/OneFlow_Example.png

{{% /section %}}
{{% section %}}
# OneFlow: Main branch
- Only long-live branch.
- Let's call it `master`.

{{% /section %}}
{{% section %}}
# OneFlow: Feature branch
- New features and bugfixes for upcoming release.
- Cooperation, can be push forced.

- Checkout from `master`.
- Merge to `master`.

{{% /section %}}
{{% section %}}
# Feature branch picture
![Feature branch][p3]

[p3]: http://endoflineblog.com/img/oneflow/feature-branch-rebase-and-merge-final.png

{{% /section %}}
{{% section %}}
# OneFlow: Release branch
- Prepare project to be released.

- Start from proper commit on `master`.
- Merge to `master`.

{{% /section %}}
{{% section %}}
# Release branch picture
![Release branch][p4]

[p4]: http://endoflineblog.com/img/oneflow/release-branch-merge-final.png

{{% /section %}}
{{% section %}}
# OneFlow: Hotfix branch
- For critical defect solutions.

- Start from last version tag.
- Merge to `master`.

{{% /section %}}
{{% section %}}
# Hotfix branch picture
![Hotfix branch][p5]

[p5]: http://endoflineblog.com/img/oneflow/hotfix-branch-merge-final.png

{{% /section %}}
{{% section %}}
# So, like that...
## Licenses
- https://opensource.org/licenses/

## Versioning
- https://semver.org/
- https://keepachangelog.com/en/1.0.0/

## Branching model
- http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
- https://nvie.com/posts/a-successful-git-branching-model/

## Contributed images
- http://numergent.com/images/dev/git-flow.png
- http://gitgraphjs.com/

{{% /section %}}
